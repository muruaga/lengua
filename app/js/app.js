(function($) {
  var $body;

  $(document).ready(function () {
    $body = $('body');

    var words = data.sentence.split(' ');
    var cellWidth = parseInt(100/words.length);

    var bodyStyles = document.body.style;
    bodyStyles.setProperty('--cell-width-1', cellWidth + '%');
    bodyStyles.setProperty('--cell-width-2', cellWidth + '%');
    bodyStyles.setProperty('--cell-width-3', cellWidth + '%');
    bodyStyles.setProperty('--cell-width-4', cellWidth + '%');
    bodyStyles.setProperty('--cell-width-5', cellWidth + '%');

    var $sentence = $('.sentence');
    $.each(words, function (index, word) {
      $sentence.append('<div class="word col cell-width-1 text-center">' + word + '</div>');
    });

    initCategoties();
    initFunctions();

    eventsWords();
    eventsSelButtons();
    eventsPopup();

  });

  function initCategoties() {
    var $categories = $('#categories');
    $.each(categories, function () {
      $categories.append('<option value="' + this.value + '">' + this.name + '</option>')
    });
  }

  function initFunctions() {
    var $functions = $('#functions');
    $.each(functions, function () {
      $functions.append('<option value="' + this.value + '">' + this.name + '</option>')
    });
  }

  function eventsWords() {
    $('.word').click(function () {
      var $wordSelects = $('.word.selected');
      var numSelected = $wordSelects.length;
      if ($(this).hasClass('selected')) {
        --numSelected;
        $(this).removeClass('selected');
      }
      else {
        if (numSelected === 2) {
          var $first = $wordSelects.first();
          var $last = $wordSelects.last();

          if ($(this).index() < $first.index()) {
            $first.removeClass('selected');
          }
          else {
            $last.removeClass('selected');
          }
        }

        $(this).addClass('selected');
        ++numSelected;
      }
      if (numSelected) {
        $body.addClass('selected');
      }
      else {
        $body.removeClass('selected');
      }
    });
  }


  function eventsSelButtons() {
    $('.selection-button.add').click(function () {
      $body.removeClass('selected');
      showPopup();
    });
    $('.selection-button.cancel').click(function () {
      $('body, .word').removeClass('selected');
    });
  }

  
  function showPopup() {
    $body.addClass('popup-show');
  }

  function eventsPopup() {
    $('#buttonApply').click(function () {
      $body.removeClass('selected');
      $body.removeClass('popup-show');

      addCategoryAndFunction();
    });
    $('#buttonCancel').click(function () {
      $body.removeClass('selected');
      $body.removeClass('popup-show');
    });
  }

  function addCategoryAndFunction() {
    var $wordSelects = $('.word.selected');
    var $first = $wordSelects.first();
    var $last = $wordSelects.last();
    var $catFnc = $('<div class="cat-fnc">' + $('#categories').val() + '/' + $('#functions').val() + '</div>');
    $catFnc.width($last.position().left + $last.outerWidth(true) - $first.position().left);
    $catFnc.css('left', $first.position().left);
    $('.response').append($catFnc);
  }
})(jQuery);



