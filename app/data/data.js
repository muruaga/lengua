var data = {
  sentence : 'El tutor canceló la entrevista.',
  solution: [
    {row: [
      {cols: '4', value:'Det/Act'},
      {cols: '5', value: 'Sust/N'}
    ]},
    {row: [
      {cols: '1', value: 'Det/Act'},
      {cols: '2', value: 'Sust/N'},
      {cols: '3', value: 'V/N'},
      {cols: '4-5', value: 'GN/CD'}
    ]},
    {row: [
      {cols: '1-2', value: 'GN/Suj'},
      {cols: '3-5', value: 'GV/Pred'}
    ]}
  ]
};

var categories = [
  {name: 'Sustantivo', value: 'Sust'},
  {name: 'Artículo', value: 'Art'},
  {name: 'Adjetivo', value: 'Adj'},
  {name: 'Adverbio', value: 'Adv'},
  {name: 'Verbo', value: 'V'},
  {name: 'Preposición', value: 'Prep'},
  {name: 'Grupo nominal', value: 'GN'},
  {name: 'Grupo verbal', value: 'GV'},
  {name: 'Grupo preposicional', value: 'GPrep'},
  {name: 'Grupo adjetival', value: 'GAdj'},
  {name: 'Grupo adverbia', value: 'GAdv'},
  {name: 'Oración', value: 'O'}
];

var functions = [
  {name: 'Núcleo', value: 'N'},
  {name: 'Sujeto', value: 'Suj'},
  {name: 'Predicado', value: 'Pred'},
  {name: 'Atributo', value: 'Atri'},
  {name: 'Complemento del nombre', value: 'CN'},
  {name: 'Modificador', value: 'Mod'},
  {name: 'Determinante', value: 'Det'},
  {name: 'Enlace', value: 'Enl'},
  {name: 'Término', value: 'Term'},
  {name: 'Complemento circunstancial', value: 'CC'},
  {name: 'Complemento Directo', value: 'CD'},
  {name: 'Complemento Indirecto', value: 'CI'},
  {name: 'Complemento de Régimen', value: 'CR'},
  {name: 'Complemento Predicativo', value: 'CPred'},
  {name: 'Nexo', value: 'Nex'}
];